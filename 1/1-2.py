# -*- coding:utf-8 -*-
# python 2.7
# auth 孙鹏飞
# 项目一，聚类算法分析不同国家的人口、收入等
# 层次聚类

import sys
import os
import math

# 将每个数据点作为 Hierarchical 的对象
# class 
# center 所属中心点
# left  左点
# right 右点
# flag
# distance 距center距离
class Hierarchical:
    def __init__(self, center, left=None, right=None, flag=None, distance=0.0):
        self.center = center
        self.left = left
        self.right = right
        self.flag = flag
        self.distance = distance

def traverse(node):
    if node.left == None and node.right == None:
        return [node.center]
    else:
        return traverse(node.left) + traverse(node.right)

# 计算距离
def distance(v1, v2):
    if len(v1) != len(v2):
        print sys.stderr, "invalid v1 and v2 !"
        sys.exit(1)
    distance = 0
    for i in range(len(v1)):
        distance += (v1[i] - v2[i]) ** 2
    distance = math.sqrt(distance)
    return distance

# 层次聚集和核心算法，迭代分层聚集
def hcluster(data, n):
    if len(data) <= 0:
        print sys.stderr, "invalid data"
        sys.exit(1)
    clusters = []
    for i in range(len(data)):
        clusters.append(Hierarchical(data[i],flag=i))
    distances = {}
    min_id1 = None
    min_id2 = None
    currentCluster = -100

    while(len(clusters) > n):
        minDist = 1000000000000
        for i in range(len(clusters) - 1):
            for j in range(i + 1, len(clusters)):
                # save distance, pick up speed
                if distances.get((clusters[i].flag, clusters[j].flag)) == None:
                    distances[(clusters[i].flag, clusters[j].flag)] = distance(
                        clusters[i].center, clusters[j].center)

                if distances[(clusters[i].flag, clusters[j].flag)] <= minDist:
                    min_id1 = i
                    min_id2 = j
                    minDist = distances[(clusters[i].flag, clusters[j].flag)]

        if min_id1 != None and min_id2 != None and minDist != 1000000000000:
            newCenter = [(clusters[min_id1].center[i] + clusters[min_id2].center[i]
                          )/2 for i in range(len(clusters[min_id2].center))]
            newFlag = currentCluster
            currentCluster -= 1
            newCluster = Hierarchical(
                newCenter, clusters[min_id1], clusters[min_id2], newFlag, minDist)
            del clusters[min_id2]
            del clusters[min_id1]
            clusters.append(newCluster)
    finalCluster = [traverse(clusters[i]) for i in range(len(clusters))]
    return finalCluster

# 读取文件中的数据
def loadData(filename):
    infile = open(filename, 'r')
    line = infile.readline()
    dataList = []
    tempList = []
    cityName = []
    countryName = []
    firstYearPeopleNumber = []
    du = 0
    while line:
        lineArr = line.strip().split(';')
        # print lineArr # 打印每行进行分割后的结果
        if len(lineArr) < 8:    # 如果分割后的数据少于8，说明此行数据有问题，舍弃，继续下一行
            line = infile.readline()
            continue
        peopleNumber = 0    # 第一年人口数量
        for i in range(len(lineArr)):
            if (i == 0):    # 将城市名字转换为数字
                if (lineArr[i] in cityName):
                    lineArr[i] = cityName.index(lineArr[i])
                    du = 1
                else:
                    cityName.append(lineArr[i])
                    lineArr[i] = cityName.index(lineArr[i])
                    firstYearPeopleNumber.append(lineArr[5])
                    du = 0
                tempList.append(float(lineArr[i]))    # 不将城市计入数据中，只计入国家
            elif (i == 1):  # 将国家名字转换为数字
                if (lineArr[i] in countryName):
                    lineArr[i] = countryName.index(lineArr[i])
                else:
                    countryName.append(lineArr[i])
                    lineArr[i] = countryName.index(lineArr[i])
                tempList.append(float(lineArr[i]))
            elif (i == 5):
                tempList.append(float(lineArr[i]))
            else:
                # tempList.append(float(lineArr[i]))    # 剩下的所有数据存入data中
                continue    # 跳过其它不需保存的数据
        tempList.append(float(float(lineArr[5])-float(firstYearPeopleNumber[int(tempList[0])]))) # 计算每个国家每年人口数量与第一年的差值
        # print tempList    # 打印每一行转换之后的数据
        if (du == 0):
            dataList.append(tempList)
        tempList = []
        line = infile.readline()
    print ('读取了 %d 个地区的 %d 个国家的数据' % (len(countryName), len(cityName)))
    # print ('共有 %d 条数据' % (len(dataList)))
    print ('共有 %d 条数据' % (7192))
    return dataList,countryName,cityName

# 主程序
if __name__ == '__main__':

    data,countryName,cityName = loadData('data.txt')
    finalCluster = hcluster(data,8)
    for i in range(len(finalCluster)):
        resultCounrtry = []
        resultCity = []
        print ('第 %d 个聚集类包括的地区: ' % (i+1))
        for j in range(len(finalCluster[i])):
            if (not countryName[int(finalCluster[i][j][1])] in resultCounrtry):
                resultCounrtry.append(countryName[int(finalCluster[i][j][1])])
        print resultCounrtry
        print ('第 %d 个聚集类包括的国家：' % (i+1))
        for j in range(len(finalCluster[i])):
            if (not cityName[int(finalCluster[i][j][0])] in resultCity):
                resultCity.append(cityName[int(finalCluster[i][j][0])])
        print resultCity