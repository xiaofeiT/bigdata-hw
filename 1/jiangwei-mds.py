from __future__ import print_function
import numpy as np
# import matplotlib.pyplot as plt
# import pylab as pl
from sklearn.decomposition import PCA
# from mpl_toolkits.mplot3d import Axes3D
from scipy import io as spio
from sklearn.decomposition import pca
from sklearn.preprocessing import StandardScaler
# import pandas as pd
from sklearn.manifold import MDS
import math


class CityData:
    cityName = ""
    cityLocation = ""
    year = 1990
    healthPoint = 0
    income = 0
    citizenNum = 0
    longitude = 0
    latitude = 0

    def gatherAttr(self):
        return ",".join("{}={}"
                        .format(k, getattr(self, k))
                        for k in self.__dict__.keys())

    def __str__(self):
        return "[{}]".format(self.gatherAttr())


class CityRateData:
    cityName = ""
    cityLocation = ""
    healthRate = 0
    incomeRate = 0
    citizenRate = 0
    longitude = 0
    latitude = 0

    def gatherAttr(self):
        return ",".join("{}={}"
                        .format(k, getattr(self, k))
                        for k in self.__dict__.keys())

    def __str__(self):
        return "[{}]".format(self.gatherAttr())


def loadData():
    file = open('data.txt', 'r')
    line = file.readline()
    dataset = []
    while line:
        data = line.split(";")
        cityData = CityData()
        cityData.cityName = data[0]
        cityData.cityLocation = data[1]
        cityData.year = data[2]
        cityData.income = float(data[3])
        cityData.healthPoint = float(data[4])
        cityData.citizenNum = float(data[5])
        cityData.longitude = data[6]
        cityData.latitude = data[7]
        dataset.append(cityData)
        line = file.readline()
    rateset = []
    for i in range(0, 124):
        rateData = CityRateData()
        rateData.cityName = dataset[i * 58].cityName
        rateData.cityLocation = dataset[i * 58].cityLocation
        rateData.latitude = dataset[i * 58].latitude
        rateData.longitude = dataset[i * 58].longitude
        rateData.citizenRate = (dataset[i * 58 + 57].citizenNum - dataset[i * 58].citizenNum) / dataset[
            i * 58].citizenNum
        rateData.healthRate = (dataset[i * 58 + 57].healthPoint - dataset[i * 58].healthPoint) / dataset[
            i * 58].healthPoint
        rateData.incomeRate = (
            dataset[i * 58 + 57].income - dataset[i * 58].income) / dataset[i * 58].income
        rateset.append(rateData)
    return rateset


def mds(dataset):
    numset = [[x.citizenRate, x.healthRate, x.incomeRate] for x in dataset]
    arr = np.array(numset)
    mds = MDS(n_components=2)
    mds.fit(arr)
    NEWDATA = mds.fit_transform(arr)
    X = mds.embedding_
    print(len(X))


def pca(dataset):
    numset = [[x.citizenRate, x.healthRate, x.incomeRate] for x in dataset]
    arr = np.array(numset)
    pca = PCA(n_components=2)
    pca.fit(arr)
    X = pca.fit_transform(arr)
    # print(X)
    # print(pca.explained_variance_ratio_)
    # print(pca.singular_values_)


if __name__ == '__main__':
    data = loadData()
    # mds(data)
    pca(data)
