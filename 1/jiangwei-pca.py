# _*_coding:utf-8_*_
# python2.7
# PCA降维算法

import numpy as np
import matplotlib.pyplot as plt
from sklearn import datasets,decomposition,manifold

def load_data():
    iris=datasets.load_iris()
    # print iris.data
    # print iris.target
    return iris.data,iris.target

# 读取文件中的数据
def loadData(filename):
    infile = open(filename, 'r')
    line = infile.readline()
    dataList = []
    tempList = []
    cityName = []
    countryName = []
    firstYearPeopleNumber = []
    du = 0
    while line:
        lineArr = line.strip().split(';')
        # print lineArr # 打印每行进行分割后的结果
        if len(lineArr) < 8:    # 如果分割后的数据少于8，说明此行数据有问题，舍弃，继续下一行
            line = infile.readline()
            continue
        peopleNumber = 0    # 第一年人口数量
        for i in range(len(lineArr)):
            if (i == 0):    # 将城市名字转换为数字
                if (lineArr[i] in cityName):
                    lineArr[i] = cityName.index(lineArr[i])
                    du = 1
                else:
                    cityName.append(lineArr[i])
                    lineArr[i] = cityName.index(lineArr[i])
                    firstYearPeopleNumber.append(lineArr[5])
                    du = 0
                tempList.append(float(lineArr[i]))    # 不将城市计入数据中，只计入国家
            elif (i == 1):  # 将国家名字转换为数字
                if (lineArr[i] in countryName):
                    lineArr[i] = countryName.index(lineArr[i])
                else:
                    countryName.append(lineArr[i])
                    lineArr[i] = countryName.index(lineArr[i])
                tempList.append(float(lineArr[i]))
            elif (i == 5):
                tempList.append(float(lineArr[i]))
            else:
                # tempList.append(float(lineArr[i]))    # 剩下的所有数据存入data中
                continue    # 跳过其它不需保存的数据
        tempList.append(float(float(lineArr[5])-float(firstYearPeopleNumber[int(tempList[0])]))) # 计算每个国家每年人口数量与第一年的差值
        print tempList    # 打印每一行转换之后的数据
        if (du == 0):
            dataList.append(tempList)
        tempList = []
        line = infile.readline()
    print ('读取了 %d 个地区的 %d 个国家的数据' % (len(countryName), len(cityName)))
    # print ('共有 %d 条数据' % (len(dataList)))
    print ('共有 %d 条数据' % (7192))
    # return dataList,countryName,cityName
    return dataList

def test_PCA(*data):
    X,Y=data
    pca=decomposition.PCA(n_components=None)
    pca.fit(X)
    print("explained variance ratio:%s"%str(pca.explained_variance_ratio_))

def plot_PCA(*data):
    X,Y=data
    pca=decomposition.PCA(n_components=3)
    pca.fit(X)
    X_r=pca.transform(X)
 #   print(X_r)

    fig=plt.figure()
    ax=fig.add_subplot(1,1,1)
    colors=((1,0,0),(0,1,0),(0,0,1),(0.5,0.5,0),(0,0.5,0.5),(0.5,0,0.5),(0.4,0.6,0),(0.6,0.4,0),(0,0.6,0.4),(0.5,0.3,0.2),)
    for label,color in zip(np.unique(Y),colors):
        position=Y==label
  #      print(position)
        ax.scatter(X_r[position,0],X_r[position,1],label="target=%d"%label,color=color)
    ax.set_xlabel("X[0]")
    ax.set_ylabel("Y[0]")
    ax.legend(loc="best")
    ax.set_title("PCA")
    plt.show()
# X,Y = loadData('data1.txt')
X,Y=load_data()
test_PCA(X,Y)
plot_PCA(X,Y)