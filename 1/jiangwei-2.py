# -*- coding: utf-8 -*-
 
from math import *
import random as rd
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
 
def zeroMean(dataMat): 
    meanVal = np.mean(dataMat,axis = 0)#计算该轴上的统计值（0为列，1为行）
    newData = dataMat - meanVal
    return newData,meanVal
 
def pca(dataMat,percent=0.99):
    '''
    若rowvar=0，说明传入的数据一行代表一个样本，若非0
    说明传入的数据一列代表一个样本。因为newData每一行代表一个样本，所以将rowvar设置为0 '''
    newData,meanVal=zeroMean(dataMat)
    covMat=np.cov(newData,rowvar=0)
    eigVals,eigVects = np.linalg.eig(np.mat(covMat))
    n=percentage2n(eigVals,percent)          #要达到percent的方差百分比，需要前n个特征向量
    print str(n) + u"vectors"
    eigValIndice=np.argsort(eigVals)            #对特征值从小到大排序  
    n_eigValIndice=eigValIndice[-1:-(n+1):-1]   #最大的n个特征值的下标  
    n_eigVect=eigVects[:,n_eigValIndice]        #最大的n个特征值对应的特征向量  
    lowDDataMat=newData * n_eigVect               #低维特征空间的数据  
    reconMat=(lowDDataMat * n_eigVect.T) + meanVal  #重构数据  
    return reconMat,lowDDataMat,n
 
def percentage2n(eigVals,percentage):  
    sortArray=np.sort(eigVals)   #升序  
    sortArray=sortArray[-1::-1]  #逆转，即降序  
    arraySum=sum(sortArray)  
    tmpSum=0 
    num=0 
    for i in sortArray:  
        tmpSum += i  
        num += 1
        if tmpSum >= arraySum * percentage:  
            return num 

# 读取文件中的数据
def loadData(filename):
    infile = open(filename, 'r')
    line = infile.readline()
    dataList = []
    tempList = []
    cityName = []
    countryName = []
    firstYearPeopleNumber = []
    du = 0
    while line:
        lineArr = line.strip().split(';')
        # print lineArr # 打印每行进行分割后的结果
        if len(lineArr) < 8:    # 如果分割后的数据少于8，说明此行数据有问题，舍弃，继续下一行
            line = infile.readline()
            continue
        peopleNumber = 0    # 第一年人口数量
        for i in range(len(lineArr)):
            if (i == 0):    # 将城市名字转换为数字
                if (lineArr[i] in cityName):
                    lineArr[i] = cityName.index(lineArr[i])
                    du = 1
                else:
                    cityName.append(lineArr[i])
                    lineArr[i] = cityName.index(lineArr[i])
                    firstYearPeopleNumber.append(lineArr[5])
                    du = 0
                tempList.append(float(lineArr[i]))    # 不将城市计入数据中，只计入国家
            elif (i == 1):  # 将国家名字转换为数字
                if (lineArr[i] in countryName):
                    lineArr[i] = countryName.index(lineArr[i])
                else:
                    countryName.append(lineArr[i])
                    lineArr[i] = countryName.index(lineArr[i])
                tempList.append(float(lineArr[i]))
            elif (i == 5):
                tempList.append(float(lineArr[i]))
            else:
                # tempList.append(float(lineArr[i]))    # 剩下的所有数据存入data中
                continue    # 跳过其它不需保存的数据
        tempList.append(float(float(lineArr[5])-float(firstYearPeopleNumber[int(tempList[0])]))) # 计算每个国家每年人口数量与第一年的差值
        # print tempList    # 打印每一行转换之后的数据
        if (du == 0):
            dataList.append(tempList)
        tempList = []
        line = infile.readline()
    print ('读取了 %d 个地区的 %d 个国家的数据' % (len(countryName), len(cityName)))
    # print ('共有 %d 条数据' % (len(dataList)))
    print ('共有 %d 条数据' % (7192))
    return dataList,countryName,cityName

if __name__ == '__main__':
    # data = np.random.randint(10,100,size = (124,4))
    data = loadData('data1.txt')
    fig = plt.figure()
    ax = plt.subplot(111,projection='3d')
    ax.scatter(data[0],data[1],data[2],c='y') #绘制数据点
    ax.set_zlabel('Z') #坐标轴
    ax.set_ylabel('Y')
    ax.set_xlabel('X')
    fin = pca(data,0.9)
    mat =fin[1]
    print mat[0]
    # for i in range(len(mat)-2):
    #     ax.scatter(mat[i],mat[i+1],mat[i+2],c='y') #绘制数据点
    plt.show()