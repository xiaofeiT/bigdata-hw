# _*_coding:utf-8_*_
# python2.7
# auto 孙鹏飞
# PCA降维算法

import os
import numpy as np
import matplotlib.pylab as plt

# def dataLoader(file):
#     return np.array([ i.split() for i in open(file)],dtype="float")
# 读取文件中的数据
def dataLoader(filename):
    infile = open(filename, 'r')
    line = infile.readline()
    dataList = []
    tempList = []
    cityName = []
    countryName = []
    firstYearPeopleNumber = []
    du = 0
    while line:
        lineArr = line.strip().split(';')
        # print lineArr # 打印每行进行分割后的结果
        if len(lineArr) < 8:    # 如果分割后的数据少于8，说明此行数据有问题，舍弃，继续下一行
            line = infile.readline()
            continue
        peopleNumber = 0    # 第一年人口数量
        for i in range(len(lineArr)):
            if (i == 0):    # 将城市名字转换为数字
                if (lineArr[i] in cityName):
                    lineArr[i] = cityName.index(lineArr[i])
                    du = 1
                else:
                    cityName.append(lineArr[i])
                    lineArr[i] = cityName.index(lineArr[i])
                    firstYearPeopleNumber.append(lineArr[5])
                    du = 0
                tempList.append(float(lineArr[i]))    # 不将城市计入数据中，只计入国家
            elif (i == 1):  # 将国家名字转换为数字
                if (lineArr[i] in countryName):
                    lineArr[i] = countryName.index(lineArr[i])
                else:
                    countryName.append(lineArr[i])
                    lineArr[i] = countryName.index(lineArr[i])
                tempList.append(float(lineArr[i]))
            elif (i == 5):
                tempList.append(float(lineArr[i]))
            else:
                # tempList.append(float(lineArr[i]))    # 剩下的所有数据存入data中
                continue    # 跳过其它不需保存的数据
        tempList.append(float(float(lineArr[5])-float(firstYearPeopleNumber[int(tempList[0])]))) # 计算每个国家每年人口数量与第一年的差值
        # print tempList    # 打印每一行转换之后的数据
        if (du == 0):
            dataList.append(tempList)
        tempList = []
        line = infile.readline()
    print ('读取了 %d 个地区的 %d 个国家的数据' % (len(countryName), len(cityName)))
    # print ('共有 %d 条数据' % (len(dataList)))
    print ('共有 %d 条数据' % (7192))
    return dataList,countryName,cityName

def pca(dataSet,rank=0):
    means = np.mean(dataSet,axis=0)
    dataSet = dataSet - means #去中心化 ,使得E(X),E(Y),... =0, 方便求方差
    #plt.scatter(*dataSet.T)
    lenth = dataSet.shape[0]
    if rank == 0:
        rank = dataSet.shape[1]+1
    dataSet = np.matrix(dataSet)
    covMat = dataSet.T*dataSet/(lenth-1)
    eigenVals, eigenVec = np.linalg.eigh(covMat)
    pick_id = eigenVals.argsort()[::-1][:rank] #从小到大排序,返回数组序号,然后倒着取,取前r个
    eigenVec_picked = eigenVec[:,pick_id]#取出排序后的特征向量
    #print(eigenVec_picked)
    #print(eigenVals, eigenVec)
    Y= dataSet*eigenVec_picked; #相当于求了个新坐标系下的数据分布
    
    return Y, eigenVec_picked, means
      
datas = dataLoader('data1.txt')
Y,P,means = pca(datas,1)
plt.scatter(*datas.T) #原数据
plt.scatter(*( Y*P.T+np.tile(means,(len(Y),1)) ).T,color='r')#降维以后的数据变换到原来的坐标下...
