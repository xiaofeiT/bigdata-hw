# 项目二的说明及要求

本次作业要求针对第十九届中央委员的统计数据进行分析，展示说明这204名委员(或部分)的属性特征，或依据委员个人履历分析其调任升迁的生涯轨迹，或展示委员在职场上是否曾有共事等信息。

数据统计自网络，包括委员基本信息（姓名、性别、民族、出生地、出生年月、毕业院校、专业背景、现任职务）和履历。履历为字符串类型，每人的履历包括多条记录，记录之间用换行符隔开。

其中每条记录包括时间和部分描述性记录，中间由逗号隔开。

时间格式统一为“起始时间(yy或yy.mm，例：1987)——结束时间（yy或yy.mm，例：1989.02）”，如起始时间缺失则表示始年不详，结束时间缺失则表示至今。

## 任务1

- 1)基本信息统计。请按照基本信息如性别、年龄、籍贯等方面对数据进行统计分析。统计角度应选取合理，具有一定的新意。

- 2)履历主题分析。使用LDA主题模型，寻找文本中所蕴含的主题。请注意选择合理的分词，停词表等预处理技术。展示每个人员在不同主题的分布，以及每个主题的在关键词上的分布。

## 任务2

- 1)提取履历中的地点和时间信息。抽取不同人员的履历轨迹。

- 2)分析不同人员之间的关系（可以思考的角度包括基本信息、履历经历等）。

- 3)任何其他的模式。

## 最终提交形式为：

- 1)源代码

- 2)实验报告

## 实验报告应包括如下内容：

- 1)实验思路：包括但不仅限于实验预期完成的任务、实验思路；

- 2)分析过程：包括但不仅限于参数说明、算法说明、分词算法、停词表的选择、关键代码；

- 3)分析结果：以图的形式进行呈现，例如散点图、线图等，必要时请用文字等其他形式加以解释说明；

- 截止日期为：2018年4月27日(第八周周五)24:00。请以“学号+姓名+HW2”的形式命名word文档，并发送至邮箱`Zhaodanning@tju.edu.cn`和`vsssilee@tju.edu.cn`
