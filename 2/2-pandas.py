# _*_coding:utf-8_*_
#

import json
from collections import defaultdict

import jieba
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import regex as re
from gensim import corpora
from gensim.models import LdaModel, TfidfModel

plt.rcParams['font.sans-serif'] = ['Ubuntu']  # 用来正常显示中文标签
plt.rcParams['axes.unicode_minus'] = False  # 解决保存图像是负号'-'显示为方块的问题

# return dataFrame 类型的数据
def loadFile(fileName):
    data = pd.DataFrame(pd.read_excel(fileName, encoding='utf-8'))
    return data

# 输入统计的列名字,输出统计信息
def tongji(cloName):
    dataList = list(peoplePandaData[cloName])
    stringArr = []
    numberArr = []
    for i in range(len(dataList)):
        if (dataList[i] in stringArr):
            numberArr[stringArr.index(dataList[i])] += 1
        else:
            stringArr.append(dataList[i])
            numberArr.append(0)
            numberArr[stringArr.index(dataList[i])] += 1
    for i in range(len(stringArr)):
        if (stringArr[i] != stringArr[i]):
            stringArr[i] = u'无记录'
        stringArr[i] = stringArr[i].encode('utf-8')
        print ('%s ： %d ' % (stringArr[i], numberArr[i]))
    # 数量柱状图
    plt.bar(stringArr,numberArr)
    plt.show()

# 主要用来统计出生地
def tongjiLocation(cloName):
    dataList = list(peoplePandaData[cloName])
    stringArr = []
    numberArr = []
    for i in range(len(dataList)):
        if (dataList[i] != dataList[i]):
            dataList[i] = u'无记录'
        dataList[i] = dataList[i][0:2].encode('utf-8')
        if (dataList[i] in stringArr):
            numberArr[stringArr.index(dataList[i])] += 1
        else:
            stringArr.append(dataList[i])
            numberArr.append(0)
            numberArr[stringArr.index(dataList[i])] += 1
    for i in range(len(stringArr)):

        print ('%s ： %d ' % (stringArr[i], numberArr[i]))
    # 数量柱状图
    plt.bar(strs
    
# We would like to return list of tuple ( Date_start, Date_end, Province ) if lack, fill None
def analysis_trace(text):
    trace_list = []
    lines = str(text).splitlines()
    for line in lines:
        time_range = line[0:line.find(",")]
        if time_range.find('—') != -1:
            [start_date, end_date] = time_range.split('—')
            start_date = parse_time(start_date)
            end_date = parse_time(end_date)
            rest_place = line[line.find(",") + 1:]
            rest_place = [
                place for place in province_city if place in rest_place]
            if len(rest_place) == 0:
                rest_place = None
            trace_list.append(
                {"开始时间": start_date, "结束时间": end_date, "地点": rest_place})
    return trace_list

if __name__ == '__main__':

    peoplePandaData = loadFile('data.xlsx')
    locationPandaData = loadFile('lonlat.xlsx')

    # --------------男女人数及男女比-------------------------
    xingbieList = list(peoplePandaData[u'性别'])
    manNumber = 0
    womanNumber = 0
    for i in range(len(xingbieList)):
        if (xingbieList[i] == u'男'):
            manNumber += 1
        elif(xingbieList[i] == u'女'):
            womanNumber += 1
        else:
            print ('第 %d 行数据性别出错' % (i+1))
    # 男女数量画图
    # plt.bar(['man','woman'], [manNumber,womanNumber])
    # plt.show()
    print '--------男女人数统计------'
    print ('男人的数量为：%d ，女人的数量为：%d，男女比约为：%d ：1' %
           (manNumber, womanNumber, int(manNumber/womanNumber)))

    # ---------------年龄信息统计------------------------
    # ageList = list(peoplePandaData[u'年龄'])
    # print ageList

    # ---------------民族统计---------------------------
    minzuList = list(peoplePandaData[u'民族'])
    minzuNameArr = []
    minzuNumArr = []
    for i in range(len(minzuList)):
        if (minzuList[i] in minzuNameArr):
            minzuNumArr[minzuNameArr.index(minzuList[i])] += 1
        else:
            minzuNameArr.append(minzuList[i])
            minzuNumArr.append(0)
            minzuNumArr[minzuNameArr.index(minzuList[i])] += 1
    print '----------民族统计--------'
    for i in range(len(minzuNameArr)):
        minzuNameArr[i] = minzuNameArr[i].encode('utf-8')
        print ('%s ： %d ' % (minzuNameArr[i], minzuNumArr[i]))
    # 各个民族数量柱状图
    # plt.bar(minzuNameArr,minzuNumArr)
    # plt.show()

    # ---------------专业背景统计------------------------
    cloName = u'专业背景'
    print '-----------专业背景统计--------'
    tongji(cloName)

    # -------------出生地统计---------------------------
    cloName = u'出生地'
    print '------------出生地统计---------'
    tongjiLocation(cloName)

    # -------------LDA履历分析-------------------------
    resume = list(peoplePandaData[u'履历'])  # 获取履历LIST
    name = list(peoplePandaData[u'姓名'])  # 获取姓名LIST
    for i in range(len(resume)):
        resume[i] = resume[i].encode('utf-8')
        name[i] = name[i].encode('utf-8')
    stoplist = ['.', ',', '、', '：', '..', '（', '）', '', '—', ']', '[', '。', ' ', '；', '\n', '—', '\u3000', "啊", "鄙人", "不独", "不问", "此外", "地", "反过来", "果真", "阿", "彼", "不管", "不只", "从", "第", "反过来", "过", "哎", "彼此", "不光", "朝", "从而", "叮咚", "说", "哈", "哎", "呀", "边", "不过", "朝着", "打", "对", "反之", "哈哈", "哎哟", "别", "不仅", "趁", "待", "对于", "非但", "呵", "唉", "别的", "不拘", "趁着", "但", "多", "非徒", "和", "俺", "别说", "不论", "乘", "但是", "多少", "否则",
                "何", "俺们", "并", "不怕", "冲", "当", "而", "嘎", "何处", "按", "并且", "不然", "除", "当着", "而况", "嘎登", "何况", "按照", "不比", "不如", "除此之", "到", "而且", "该", "何时", "吧", "不成", "不特", "外", "得", "而是", "赶", "嘿", "吧哒", "不单", "不惟", "除非", "的", "而外", "个", "哼", "把", "不但", "跟", "除了", "的话", "而言", "各", "哼唷", "罢了", "各自", "故", "此", "等", "而已", "各个", "呼哧", "被", "给", "故此", "此间", "等等", "尔后", "各位", "乎", "关于", "根据", "固然", "管", "归", "果然", "各种", "哗"]
    texts = [[word for word in jieba.cut(re.sub('\n|\d*|—|\u3000', '', str(document)), cut_all=False) if word not in stoplist]
             for document in resume]
    # 到此为止，我们完成了分词任务，为了简单我们使用中文分词Python包jieba，使用了精确模式来进行划分
    # 去掉只出现一次的单词
    frequency = defaultdict(int)
    for text in texts:
        for token in text:
            frequency[token] += 1
    dictionary = corpora.Dictionary(texts)   # 生成词典
    dictionary.filter_extremes(no_below=10, no_above=0.4)
    corpus = [dictionary.doc2bow(text) for text in texts]
    tfidf = TfidfModel(corpus)
    corpus_tfidf = tfidf[corpus]
    TOPIC_NUMBER = 20
    EM_ITERATIONS = 200
    lda = LdaModel(corpus=corpus_tfidf, id2word=dictionary,
               num_topics=TOPIC_NUMBER, iterations=EM_ITERATIONS)
    print '-----------LDA模型结果------------'
    print "主题在关键字上的分布："
    for i in range(TOPIC_NUMBER):
        # print ("主题 " , str(i) + ": " ,json.dumps(lda.print_topic(i), encoding="UTF-8", ensure_ascii=False))
        print ('主题 %d 分布：%s' % (i,lda.print_topic(i).encode('utf-8')))
    print("每个人在主题上的分布：")
    for i in range(len(name)):
        print(name[i] + ": " + str(lda[corpus[i]]))
    
    # ---------------时间地点抽取-------------------
    print("----------抽取时间地点--------------------")
    # 根据数据中时间的规则提取，使用匹配的方式来抽取地点
    allPeopleTimeLocationList = []
    # 胡泽君的履历读取出错，做删除处理
    for i in range(len(resume)):
        resume[:resume[i].index('\n')]

    peoplePandaData["履历细节"] = peoplePandaData["履历"].map(lambda text: analysis_trace(text))

    
