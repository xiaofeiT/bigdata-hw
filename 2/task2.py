# _*_coding:utf-8_*_
# python
# LAD模型
from sklearn.decomposition import LatentDirichletAllocation
import pandas as pd
import numpy as np
import datetime

from datetime import datetime, timedelta, date
# from myutil import is_int, is_datetime_array, parse_time
from gensim import corpora
from gensim.models import LdaModel, TfidfModel
from collections import defaultdict
import regex as re
import jieba
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
# ----- For Province & City Used, Prepared -----
area = pd.read_excel("lonlat.xlsx")
area[u'省份'] = area[u'省份'].map(lambda prov: str(prov).replace(u'省','').replace(u'市','').replace(u'壮族自治区','').replace(u'回族自治区','').replace(u'维吾尔','').replace(u'自治区',''))
area[u"地市"] = area[u"地市"].map(lambda city: str(city).replace(u"市","").replace(u"盟","").replace(u"朝鲜族自治州","").replace(u"地区","").replace(u"土家族苗族自治州","").replace(u"藏族羌族自治州","").replace(u"藏族自治州","").replace(u"彝族自治州","").replace(u"苗族侗族自治州","").replace(u"依族苗族自治州","").replace(u"尼族自治县",""))
area[u"地市"] = area[u"地市"].map(lambda city: str(city).replace(u"彝族自治州", "").replace(u"哈尼族彝族自治州", "").replace(u"壮族苗族自治州","").replace(u"傣族自治州","").replace(u"白族自治州","").replace(u"傣族景颇族自治州","").replace(u"傈僳族自治州","").replace(u"回族自治州","").replace(u"蒙古族藏族自治州","").replace(u"蒙古自治州","").replace(u"哈萨克自治州",""))
province = area[u"省份"]
city = area[u"地市"]
# We get the province for address analysis
province = province.unique().tolist()
city = city.unique().tolist()
province_city = province + city
# ----- END -----

# 数据补齐
# 156,倪岳峰，出生日期原数据有错误，修改了原来数据
# 28,补齐
# 51，刘万龙缺少日期，做补齐处理

origin_data = pd.read_excel("./data.xlsx")
print int(origin_data[u'出生日期'][0])
print origin_data[u'出生日期'][1]
# data = origin_data[is_datetime_array(origin_data["出生日期"])]
# data = origin_data[origin_data[u"出生日期"]]
data = origin_data
data = data.dropna()
data[u"年龄"] = data[u'出生日期'].map(lambda born: date.today().year - born.year - ((date.today().month, date.today().day) < (born.month, born.day)))
data[u"籍贯"] = data[u"出生地"]
select_data = data[[u"年龄", u"性别", u"籍贯", u"民族", u"专业背景", u"毕业院校"]]

# 后续应该继续处理籍贯的一些情况，各种格式都不一致
# 对于籍贯，格式不一致得部分手动清理

# 1.1 TODO: Need Finished
# 然后我们可以对于男女比例这个事情进行分析
# 另外也可以对年龄和专业之间的关系进行分析
# 不过主要是统计的话，关心男女、年龄、民族比例就可以了

print("----------------------------------------")

sex = data["性别"]
print(sex.value_counts())
age = data["年龄"]
print(age.value_counts())
nation = data["民族"]
print(nation.value_counts())

# 1.2 TODO: LDA
# 首先获取履历，然后对履历进行LDA主题分析
# Here we use original_data, since all people has resume
print("")
print("----------------------------------------")
resume = origin_data["履历"]
name = origin_data["姓名"]
stoplist = ['.', ',', '、', '：', '..', '（', '）', '', '—', ']', '[', '。', ' ', '；', '\n', '—', '\u3000', "啊", "鄙人", "不独", "不问", "此外", "地", "反过来", "果真", "阿", "彼", "不管", "不只", "从", "第", "反过来", "过", "哎", "彼此", "不光", "朝", "从而", "叮咚", "说", "哈", "哎", "呀", "边", "不过", "朝着", "打", "对", "反之", "哈哈", "哎哟", "别", "不仅", "趁", "待", "对于", "非但", "呵", "唉", "别的", "不拘", "趁着", "但", "多", "非徒", "和", "俺", "别说", "不论", "乘", "但是", "多少", "否则", "何", "俺们", "并", "不怕", "冲", "当", "而", "嘎", "何处", "按", "并且", "不然", "除", "当着", "而况", "嘎登", "何况", "按照", "不比", "不如", "除此之", "到", "而且", "该", "何时", "吧", "不成", "不特", "外", "得", "而是", "赶", "嘿", "吧哒", "不单", "不惟", "除非", "的", "而外", "个", "哼", "把", "不但", "跟", "除了", "的话", "而言", "各", "哼唷", "罢了", "各自", "故", "此", "等", "而已", "各个", "呼哧", "被", "给", "故此", "此间", "等等", "尔后", "各位", "乎", "关于", "根据", "固然", "管", "归", "果然", "各种", "哗"]
texts = [[word for word in jieba.cut(re.sub('\n|\d*|—|\u3000', '', str(document)), cut_all=False) if word not in stoplist]
         for document in resume]
# 到此为止，我们完成了分词任务，为了简单我们使用中文分词Python包jieba，使用了精确模式来进行划分
# 去掉只出现一次的单词
frequency = defaultdict(int)
for text in texts:
    for token in text:
        frequency[token] += 1

dictionary = corpora.Dictionary(texts)   # 生成词典
dictionary.filter_extremes(no_below=10, no_above=0.4)
corpus = [dictionary.doc2bow(text) for text in texts]
tfidf = TfidfModel(corpus)
corpus_tfidf = tfidf[corpus]
TOPIC_NUMBER = 20
EM_ITERATIONS = 200
lda = LdaModel(corpus=corpus_tfidf, id2word=dictionary, num_topics=TOPIC_NUMBER, iterations=EM_ITERATIONS)
# Here Actually we have the LDA model
# And then We need to predict each one

print("主题在关键字上的分布")
for i in range(TOPIC_NUMBER):
    print("主题 " + str(i) + ": " + lda.print_topic(i))

print("")
print("每个人在主题上的分布")
for i in range(len(name)):
    print(name[i] + ": " + str(lda[corpus[i]]))


# 2.1 TODO: 提取履历的地点和时间信息
print("----------------------------------------")
# 时间比较好抽取已经告诉我们是什么规则了，地点就只能依靠匹配来做了
# 所以地点应该并不重要
resume = origin_data["履历"]
name = origin_data["姓名"]

# We would like to return list of tuple ( Date_start, Date_end, Province ) if lack, fill None
def analysis_trace(text):
    trace_list = []
    lines = str(text).splitlines()
    for line in lines:
        time_range = line[0:line.find(",")]
        if time_range.find('—') != -1:
            [start_date, end_date] = time_range.split('—')
            start_date = parse_time(start_date)
            end_date = parse_time(end_date)
            rest_place = line[line.find(",") + 1:]
            rest_place = [place for place in province_city if place in rest_place]
            if len(rest_place) == 0:
                rest_place = None
            trace_list.append({"开始时间": start_date, "结束时间": end_date, "地点": rest_place})
    return trace_list


# 胡泽君的履历读取出错，做删除处理
for i in range(len(resume)):
    print(name[i] + ': ' + str(analysis_trace(resume[i])))

#We also put those information into tables for further explaination
data["履历细节"] = data["履历"].map(lambda text: analysis_trace(text))
# 2.2 TODO: 分析不同人员之间的关系
print("")
print("----------------------------------------")
print("作业2-1")
# 比如处于统一地方的人员
# Since "灵寿" in not a province. In order to simplify the problem, we ignore this line

# We ignore this part since we use similarity between resume to get the similar name pairs
# data = data[data["籍贯"]!="灵寿"]
# data["省份"] = data["籍贯"].map(lambda var: [i for i in province if i in var or i == var][0])
# people_with_prov = data[["姓名", "省份"]]

# 为了更好的度量两份履历之间的相似程度，我们采用了类似Jaccard的度量
# 对于每一项，只要开始与结束有重叠（只考虑年份，简单起见），同时地点有交集，就算有一段类似经历，只要类似经历大于等于1即可，就说明两种之间有关联
# 这里其实对于没有开始时间的有一些放松要求，就是只要如果两个都没有开始时间的但是地点相同的经历，就可以算作一次相同
# 对于没有结束时间同样如此，但是其实也比较自然，因为也说明他们共事过，并且由于年龄差的不太大，所以可以接受
def judge_time_overlap(start_year_1, end_year_1, start_year_2, end_year_2):
    if start_year_1 == None:
        start_year_1 = datetime(1990, 1, 1).date()
    else:
        start_year_1 = start_year_1
    if start_year_2 == None:
        start_year_2 = datetime(1990, 1, 1).date()
    else:
        start_year_2 = start_year_2
    if end_year_1 == None:
        end_year_1 = datetime.now().date()
    else:
        end_year_1 = end_year_1
    if end_year_2 == None:
        end_year_2 = datetime.now().date()
    else:
        end_year_2 = end_year_2
    return not (end_year_1 < start_year_2 or end_year_2 < start_year_1)

def judge_place_overlap(place_list_1, place_list_2):
    if place_list_1 == None or place_list_2 == None:
        return False
    else:
        return len(set(place_list_1).intersection(set(place_list_2))) != 0

def judge_overlap(resume_1, resume_2):
    return judge_time_overlap(resume_1["开始时间"],resume_1["结束时间"],resume_2["开始时间"],resume_2["结束时间"]) and judge_place_overlap(resume_1["地点"], resume_2["地点"])


OVERLAP_THRESHOLD = 2
def resume_similarity(resumes_1, resumes_2):
    overlap_number = 0
    for resume_1 in resumes_1:
        for resume_2 in resumes_2:
            if judge_overlap(resume_1, resume_2):
                overlap_number = overlap_number + 1
    return overlap_number >= OVERLAP_THRESHOLD

people_with_resume = data[["姓名", "履历细节"]]
# Although there has a warning, but actually we do want insert on the copy since we will drop it after the merge opertaion
people_with_resume['key'] = 0
people_with_resume_cross_join = people_with_resume.merge(people_with_resume, how='left', on = 'key', suffixes=("_x","_y"))
people_with_resume_cross_join.drop('key', 1, inplace=True)
people_with_resume_cross_join = people_with_resume_cross_join[people_with_resume_cross_join["姓名_x"]!=people_with_resume_cross_join["姓名_y"]]
people_with_resume_cross_join_index = list(people_with_resume_cross_join.columns).index
resume_x_index = people_with_resume_cross_join_index("履历细节_x")
resume_y_index = people_with_resume_cross_join_index("履历细节_y")
people_resume_sim_pair = people_with_resume_cross_join[[resume_similarity(row[resume_x_index], row[resume_y_index]) for index, row in people_with_resume_cross_join.iterrows()]]
people_resume_sim_pair = people_resume_sim_pair[["姓名_x", "姓名_y"]]
print(people_resume_sim_pair)