# _*_ coding:utf-8_*_
# python2.7
# 统计分析中央委员的数据特征

import pandas as pd
from openpyxl import load_workbook

# 读取xlsx文件数据
# fileName: file's name
# return data[row][col]
def loadFile(fileName):
    wb = load_workbook(filename=fileName)
    sheetsNames = wb.sheetnames
    firstSheetName = sheetsNames[0] # 读取第一个数据表
    sheet = wb[firstSheetName] # 获得第一个数据表
    print sheet.cell(row=40,column=5).value
    data = []
    col = sheet.max_column  # 最大列数
    row = sheet.max_row # 最大行数
    for i in range(40):
        if (i == 0):
            continue
        temp = []
        for j in range(9):
            # print sheet.cell(row=i+1,column=j+1).value    # 打印单元格数据
            temp.append(sheet.cell(row=i+1,column=j+1).value)   # 取值时行列数最小也要为 1，所以加1.
            data.append(temp)
    print ('已读取 %s 数据' % (fileName))
    return data,row,col



if __name__ == '__main__':
    # lonlat.xlsx 城市数据,省份0、地市1、区县2、经度3、纬度4 3180*5
    cityData,cityDataRow,cityDataCol = loadFile('lonlat.xlsx')
    # data.xlsx 人员数据，姓名0、性别1、民族2、出生地3、出生日期4、毕业院校5、专业背景6、担任职务7、履历8 217*9
    peopleData,peopleDataRow,peopleDataCol = loadFile('data.xlsx') 

    manNumber = 0
    womanNumber = 0
    for i in range(peopleDataRow):
        if (peopleData[i][1] == u'男'):
            manNumber += 1
        elif (peopleData[i][1] == u'女'):
            womanNumber += 1
        else:
            print ('第 %d 行数据性别有错' % (i+1)),peopleData[i][1]
    
    print '----------------基本信息统计分析-------------------'
    # print ('男有 %d 人，女有 %d 人，男女比为：%f 。' % (manNumber,womanNumber,float(manNumber/womanNumber)))
    print ('男有 %d 人，女有 %d 人。' % (manNumber,womanNumber))
