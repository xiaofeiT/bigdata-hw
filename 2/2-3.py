# _*_ coding:utf-8 _*_
from openpyxl import load_workbook

# 打开一个workbook
wb = load_workbook(filename="lonlat.xlsx")

# 获取当前活跃的worksheet,默认就是第一个worksheet
#ws = wb.active
# 当然也可以使用下面的方法

# 获取所有表格(worksheet)的名字
sheets = wb.sheetnames
# 第一个表格的名称
sheet_first = sheets[0]
# 获取特定的worksheet
ws = wb[sheet_first]

# 获取表格所有行和列，两者都是可迭代的
rows = ws.rows
columns = ws.columns

# 迭代所有的行
# 文件数据：3180*5
for i in range(50):
    for j in range(5):
        print ws.cell(row=i+1, column=j+1).value

# 通过坐标读取值
print ws.cell(row=1, column=1).value
