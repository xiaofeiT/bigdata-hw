class CityData:
    cityName = ""
    cityLocation = ""
    year = 1990
    healthPoint = 0
    income = 0
    citizenNum = 0
    longitude = 0
    latitude = 0

    def gatherAttr(self):
        return ",".join("{}={}"
                        .format(k, getattr(self, k))
                        for k in self.__dict__.keys())

    def __str__(self):
        return "[{}]".format(self.gatherAttr())

class CityRateData:
    cityName = ""
    cityLocation = ""
    healthRate = 0
    incomeRate = 0
    citizenRate = 0
    longitude = 0
    latitude = 0
    def gatherAttr(self):
        return ",".join("{}={}"
                        .format(k, getattr(self, k))
                        for k in self.__dict__.keys())

    def __str__(self):
        return "[{}]".format(self.gatherAttr())