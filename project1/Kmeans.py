from __future__ import print_function
from CityData import *

import math


def loadData():
    file = open('case1.txt', 'r')
    line = file.readline()
    dataset = []
    while line:
        data = line.split(";")
        cityData = CityData()
        cityData.cityName = data[0]
        cityData.cityLocation = data[1]
        cityData.year = data[2]
        cityData.income = float(data[3])
        cityData.healthPoint = float(data[4])
        cityData.citizenNum = float(data[5])
        cityData.longitude = data[6]
        cityData.latitude = data[7]
        dataset.append(cityData)
        line = file.readline()
    rateset = []
    for i in range(0, 124):
        rateData = CityRateData()
        rateData.cityName = dataset[i * 58].cityName
        rateData.cityLocation = dataset[i * 58].cityLocation
        rateData.latitude = dataset[i * 58].latitude
        rateData.longitude = dataset[i * 58].longitude
        rateData.citizenRate = (dataset[i * 58 + 57].citizenNum - dataset[i * 58].citizenNum) / dataset[
            i * 58].citizenNum
        rateData.healthRate = (dataset[i * 58 + 57].healthPoint - dataset[i * 58].healthPoint) / dataset[
            i * 58].healthPoint
        rateData.incomeRate = (dataset[i * 58 + 57].income - dataset[i * 58].income) / dataset[i * 58].income
        rateset.append(rateData)
    return rateset


def calDis(a, b):
    cr = a.citizenRate - b.citizenRate
    hr = a.healthRate - b.healthRate
    ir = a.incomeRate - b.incomeRate
    return math.sqrt(cr ** 2 + hr ** 2 + ir ** 2)


def calMid(clusters, midList):
    pos = 0
    for cluster in clusters:
        cr = 0
        hr = 0
        ir = 0
        for data in cluster:
            cr += data.citizenRate
            hr += data.healthRate
            ir += data.incomeRate
        cr /= len(cluster)
        hr /= len(cluster)
        ir /= len(cluster)
        midList[pos].citizenRate = cr
        midList[pos].healthRate = hr
        midList[pos].incomeRate = ir
        pos += 1


def kmeans(k, dataset):
    midlist = []
    cluster = [[] for x in range(k)]
    for i in range(0, k):
        midlist.append(dataset[len(dataset) / k * i])
    for data in dataset:
        min = 65535
        pos = 65535
        for j in range(0, k):
            if calDis(data, midlist[j]) < min:
                min = calDis(data, midlist[j])
                pos = j
        cluster[pos].append(data)
    calMid(cluster, midlist)
    changed = False
    pastCluster = [[] for x in range(k)]
    while not changed:
        changed = False
        for data in dataset:
            min = 65535
            pos = 65535
            for j in range(0, k):
                if calDis(data, midlist[j]) < min:
                    pos = j
                    min = calDis(data, midlist[j])
            if data not in cluster[pos]:
                changed = True
            pastCluster[pos].append(data)
        calMid(pastCluster, midlist)
        cluster = pastCluster
        pastCluster = [[] for x in range(k)]
    return cluster


def dbscan(minradius, dataset, mindata):
    visit = []
    useless = []
    resultset = []
    for i in range(len(dataset)):
        if dataset[i] in visit:
            continue
        else:
            visit.append(dataset[i])
            contain = []
            for j in range(len(dataset)):
                if calDis(dataset[i], dataset[j]) < minradius:
                    contain.append(dataset[j])
            if len(contain) < mindata:
                useless.append(dataset[i])
            else:
                cluster = contain
                for data in cluster:
                    if data in useless:
                        continue
                    cover = []
                    if data not in visit:
                        visit.append(data)
                    for j in range(len(dataset)):
                        if calDis(dataset[j], data) < minradius:
                            cover.append(dataset[j])
                    if len(cover) < mindata:
                        useless.append(data)
                    else:
                        for d in cover:
                            if d not in cluster:
                                cluster.append(d)
                resultset.append(cluster)
    return resultset


if __name__ == '__main__':
    data = loadData()
    # clusters = kmeans(3, data)
    clusters = dbscan(0.4, data, 4)
    for cluster in clusters:
        for data in cluster:
            print(data.cityName, end=" ")
        print("\n")
